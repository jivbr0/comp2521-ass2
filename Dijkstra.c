#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Dijkstra.h"
#include "PQ.h"
#include <limits.h>

void tracePath(ShortestPaths sp, int pred[], int j);
void traceSolution(ShortestPaths sp,int pred[]);

ShortestPaths dijkstra(Graph g, Vertex src){
	int nV = numVerticies(g);
	ShortestPaths *sp = malloc(sizeof(ShortestPaths));

	sp->noNodes = nV;
	sp->src = src;
	sp->dist = malloc(nV * sizeof(int));
	
	sp->pred = malloc(sizeof(PredNode *) * nV);
	int i = 0;
	for(i=0; i<nV; i++) {
		sp->pred[i] = malloc(sizeof(PredNode*));
		sp->pred[i] = NULL;
		
	}

	//Set all dist[i] to INF except dist[src] which is 0
	for(i=0; i<nV; i++) {
		if(i == src)
			sp->dist[i] = 0;
		else
			sp->dist[i] = INT_MAX;	
	}

	PQ q = newPQ();
	//Add all vertices to the PQ
	for(i=0; i<nV; i++){
		ItemPQ item ;//= NULL;
		item.key = i;
		item.value = sp->dist[i];
		addPQ(q,item);
	}
	
	
	while(!PQEmpty(q)){
		ItemPQ curr = dequeuePQ(q);
		
		AdjList l = outIncident(g,curr.key);
		while(l != NULL){
			//printf("%d ", l->w);
			unsigned int tent_d = sp->dist[curr.key] + l->weight;
			//Relax
			if(tent_d < sp->dist[l->w] && l->w != src){
				sp->dist[l->w] = tent_d;
				//printf("Print dist array: %d, %d, %d\n",sp->dist[0],sp->dist[1],sp->dist[2]);
				ItemPQ temp;
				temp.key = l->w;
				temp.value = sp->dist[l->w];
				addPQ(q,temp);

				//sp->pred[l->w] = n;
				//tempPred[l->w] = curr.key;
				
				PredNode *n = (PredNode *)malloc(sizeof(PredNode *));
				n->v = curr.key;
				n->next = NULL;
				
				sp->pred[l->w] = n;
				
				//printf("Predecessor of %d is %d\n", l->w,curr.key);	
			}
			//Multiple shortest paths
			else if(tent_d == sp->dist[l->w] && l->w != src){
				//printf("Multiple\n");
				PredNode *n = (PredNode *)malloc(sizeof(PredNode *));
				n->v = curr.key;
				n->next = NULL;
				
				sp->pred[l->w]->next = n;		
			}
			l = l->next;
		} 
		sp->pred[src] = NULL;
		
	}
	for(i=0; i<sp->noNodes; i++){
			if(sp->dist[i] > (INT_MAX-(INT_MAX/4)))
				sp->dist[i] = 0;
		}
	return *sp;
}

/*void tracePath(ShortestPaths sp, int pred[], int j){
	if(pred[j] == -1) return;
	printf(" %d ", j);
	tracePath(sp,pred,pred[j]);
	//if()
}

void traceSolution(ShortestPaths sp,int pred[]){
	//int src =sp.src;
	//printf("Vertex\tPath");
	int i;
	for (i = 0; i < sp.noNodes; i++)
	{
		printf("\n%d:",i);
		//PredNode *path->pred[i] = 
		tracePath(sp,pred,i);
	}
}*/


void  showShortestPaths(ShortestPaths sps){
	printf("%d %d %d\n",sps.dist[0],sps.dist[1],sps.dist[2] );
	int i = 0;
	printf("Node %d\n",sps.src);
	printf("  Distance\n");
	for (i = 0; i < sps.noNodes; i++) {
			if(i == sps.src)
	    	printf("    %d : X\n",i);
			else
				printf("    %d : %d\n",i,sps.dist[i]);
	}
	printf("  Preds\n");
	for (i = 0; i < sps.noNodes; i++) {
		printf("    %d : ",i);
		PredNode* curr = sps.pred[i];
		while(curr!=NULL) {
			printf("[%d]->",curr->v);
			curr = curr->next;
		}
		printf("NULL\n");
	}

}

void freeShortestPaths(ShortestPaths sp){
	//printf("Print dist array: %d, %d, %d\n",sp.dist[0],sp.dist[1],sp.dist[2]);
	//free(&sp.dist);



}


/*int main(int argc, char *arg[]){
	Graph g = newGraph(3);
	insertEdge(g,0,1,1);
	insertEdge(g,1,2,2);
	insertEdge(g,2,0,1);
	showGraph(g);
	int i=0;
	//for(i =0;i<numVerticies(g);i++){
		//ShortestPaths paths = dijkstra(g,i);
		//printf("Print dist array: %d, %d, %d\n",paths.dist[0],paths.dist[1],paths.dist[2]);
		ShortestPaths sp;
		sp = dijkstra(g,i);
		//int i;
		for(i=0; i<sp.noNodes; i++){
			printf("Distance from %d : to %d : %d\n",sp.src,i,sp.dist[i]); 
		} 
		showShortestPaths(sp);
		//freeShortestPaths(paths);
	//}


	return 0;
}*/