
CC=gcc
CFLAGS=-Wall -Werror -g

all : Dijkstra

Dijkstra : Dijkstra.o Graph.o PQ.o  
			$(CC) $(CFLAGS) -o dj Dijkstra.o Graph.o PQ.o  
Dijkstra.o : Dijkstra.c Graph.h PQ.h
			$(CC) $(CFLAGS) -c Dijkstra.c
Graph.o : Graph.c Graph.h
			$(CC) $(CFLAGS) -c Graph.c
PQ.o : PQ.c PQ.h
			$(CC) $(CFLAGS) -c PQ.c	

clean: 
		rm -f Dijkstra.o Graph.o PQ.o
