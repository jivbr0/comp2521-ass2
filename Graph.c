#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "Graph.h"

int containsEdge(Graph g, Vertex src,Vertex dest);
Graph readGraphFromFile(char *f);

typedef struct GraphRep {
	AdjList *adj; //Array of Adjacency Lists
	int nV; // #Vertices
	//int nE; // #Edges
} GraphRep;

Graph newGraph(int noNodes){
	assert(noNodes >= 0);
	//printf("G\n");
	Graph g = malloc(sizeof(GraphRep*));
	assert(g != NULL);
	g->nV = noNodes;
	//g->nE = 0;
	//Allocate memory for the array of adacency lists
	g->adj = malloc(noNodes * sizeof(AdjList));
	assert(g->adj != NULL);
	int i;
	for(i = 0; i < noNodes; i++){
		g->adj[i] = NULL;
	}
	return g;
}

void insertEdge(Graph g, Vertex src, Vertex dest, int weight){
	//assert(src < g->nV && dest < g->nV);
	//Create a new adj list node with the given vertext and weight
	//printf("Start\n");
	adjListNode *newEdge = malloc(sizeof(adjListNode));
	newEdge->w = dest;
	newEdge->weight = weight;
	newEdge->next = NULL;
	//printf("New edge(src,dest,w): (%d,%d) \n",newEdge->w,newEdge->weight);
	//If the source node doesnt have an adjacency list, create a new one
    if(g->adj[src] == NULL){
            g->adj[src] = newEdge;
    }
	else if(!containsEdge(g,src,dest)){
		AdjList l = g->adj[src];
		//Go all the way to the last node of source node's adjacency list
		while(l->next != NULL) 
			l = l->next;
		//Add the new edge to the source node's adjacency list
		l->next = newEdge;
		//g->nE ++;
	}
	//printf("End\n");
}

int containsEdge(Graph g, Vertex src,Vertex dest){
	AdjList l = (adjListNode *)malloc(sizeof(adjListNode *)) ;
	l= g->adj[src];
	while(l != NULL){
		if(l->w == dest) return 1;
		l = l->next;
	}
	return 0;
}

void removeEdge(Graph g, Vertex src, Vertex dest){
	assert(src < g->nV && dest < g->nV);
	assert(g->adj[src]);
	if(containsEdge(g,src,dest)){
		AdjList l = g->adj[src];
		while(l != NULL){
			if(l->next->w == dest){
				l->next = l->next->next;
			}
			l = l->next;
		}
	}	
}

bool adjacent(Graph g, Vertex src, Vertex dest){
	assert(src < g->nV && dest < g->nV);
	assert(g->adj[src]);
	AdjList l = g->adj[src];
	while(l != NULL){
		if(l->w == dest)
			return true;
		l = l->next;
	}
	return false;
}

int numVerticies(Graph g){
	return g->nV;
}

AdjList outIncident(Graph g, Vertex v){
	AdjList l = NULL;
	if(g->adj[v]) return g->adj[v];
	else return l;
}

AdjList inIncident(Graph g, Vertex v){
	int i;
	//list containing vertices inincident on Vertex v 
	AdjList res;
	//Loop through all the node's adjacency lists
	for(i = 0;i < g->nV; i++){
		//Skip the Vertex v's adjacency list
		if(i != v){
			AdjList l = g->adj[i];
			while(l != NULL){
				if(l->w == v){
					adjListNode *in = malloc(sizeof(adjListNode));
					in->w = i;
					in->weight = l->weight;
					in->next = NULL;
					//If res is empty
					if(!res){
						res = in; 
					}
					//If res is not empty
					else{
						adjListNode *temp = res;
						while(temp->next != NULL){
							temp = temp->next;
						}
						temp->next = in;
					}
				}
				l = l->next;	
			}
		}
	}
	return res;
}


void showGraph(Graph g){
	int i = 0;
	printf("Printing Graph: \n");
	for(i = 0; i<g->nV;i++){
		printf("Vertex %d has an outgoing link to : \n",i);
		//printf("\t");
		AdjList l = g->adj[i];
		while(l !=  NULL){
			printf("\t%d : w = %d\n",l->w,l->weight);
			l = l->next;
		}
		printf("\n");
		//i++;
	}
}	

void freeGraph(Graph g){
	int i =0;
	//Free adacency lists of all vertices
	for(i = 0;i < g->nV;i++){
		//Free individual nodes in the adacency list
		if(g->adj[i]){
			adjListNode *n = g->adj[i];
			while(n != NULL){
				adjListNode *temp = n;
				n = n->next;
				if(temp) free(temp);
			}
		}
	}
	if(g->adj) free(g->adj);
	g->nV = 0;
	if(g) {
		free(g);
	}
	g = NULL;
}

/*void readGraphFromFile(char *f){
	File *fp = fopen(f,"r");
	while(feof(fp)){
		if(fscanf())
	}


}*/


/*int main(int argc, char *argv[]){
	printf("main\n");
	Graph g = newGraph(3);
	
	insertEdge(g,2,0,6);
	printf("1\n");
	insertEdge(g,2,1,4);
	printf("2\n");
	showGraph(g);
	freeGraph(g);
	//showGraph(g);
	//int n = numVerticies(g);
	//printf("No of Vertices: %d\n",n);
	//printf("is vertex 1 adjacent to 4?: %s\n",adjacent(g,1,4) ? "yes": "no");
	//printf("is vertex 2 adjacent to 4?: %s\n",adjacent(g,2,4) ? "yes": "no");
	printf("Verices in incident to Vertex 4: \n");
	AdjList l = inIncident(g,4);
	while(l != NULL){
		printf("%d\n", l->w);
		l = l->next;
	}
	return 0;

}*/





















